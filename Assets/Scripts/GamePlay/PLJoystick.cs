using UnityEngine;
using UnityEngine.EventSystems;

public class PLJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private PLMovePlayer player;
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PLMovePlayer>();
    }
    public void OnPointerDown(PointerEventData eventData){
        if(gameObject.tag == "down")
        {
            player.moveUp = false;
            player.moveDown = true;
        }
        else
        {
            player.moveUp = true;
            player.moveDown = false;
        }
    }
    public void OnPointerUp(PointerEventData eventData){
        player.moveUp = false;
        player.moveDown = false;
    }

}
