using UnityEngine;

public class PLBoundPlayer : MonoBehaviour
{
    [SerializeField]
    private float maxY = 481f, minY = -481f;
    [SerializeField]
    private RectTransform rect;
    void Start()
    {
        rect = gameObject.GetComponent<RectTransform>();
    }
    void Update()
    {
        //Vector2 temp = new Vector2(transform.position.x, transform.position.y);
        Vector2 temp = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y);
        if(temp.y < minY)
        {
            temp.y = minY;
        }
        if(temp.y > maxY)
        {
            temp.y = maxY;
        }
        rect.anchoredPosition = temp;
        //transform.position = temp;
    }
}
