using UnityEngine;

public class PLHPWall : MonoBehaviour
{
    public GameObject gameOverPannel;
    public GameObject spawner;
    public GameObject player;
    public static float hitPointWall;
    public static PLHPWall hpWallInstance;
    public float maxhitPoint = 100f;
    public WallSlider heal;
    void Start()
    {
        hpWallInstance = this;
        hitPointWall = maxhitPoint;
    }
    public void takeHit(float damage){
        hitPointWall -= damage;
        if(hitPointWall <= 0){
            Destroy(gameObject);
            spawner.SetActive(false);
            player.SetActive(false);
            gameOverPannel.SetActive(true);
        }
    }
}
