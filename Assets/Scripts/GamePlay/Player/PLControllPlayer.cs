using UnityEngine;

public class PLControllPlayer : MonoBehaviour
{
    [SerializeField]
    private GameObject Weapon;
    [SerializeField]
    private float coolDownTime;
    private float attackTime;
    [SerializeField]
    private float damageValue;
    // [SerializeField]
    // private PLBombSkill bomb;
    [SerializeField]
    private Transform launchOffset;
    [SerializeField]
    private GameObject transformParent;
    //[SerializeField]
    // private Transform rocket;
    void Update()
    {
        if(attackTime <= Time.time)
        {
            Instantiate(Weapon,launchOffset.position, transform.rotation,transformParent.transform.parent);
            attackTime = Time.time + coolDownTime;
        }
    }
}
