using UnityEngine;
using UnityEngine.UI;

public class WallSlider : MonoBehaviour
{
    // public Slider slider;
    // public Color low;
    // public Color high;
    // public Vector3 offSet;
    // public void SetHeal(float health, float maxHealth){
    //     //slider.gameObject.SetActive(health < maxHealth);
    //     slider.value = health;
    //     slider.maxValue = maxHealth;
    //     Debug.Log("Health : " + health);
    //     Debug.Log("Slider : " + slider.value);
    //     Debug.Log("Max Slider : " + slider.maxValue);

    //     slider.fillRect.GetComponentInChildren<Image>().color = Color.Lerp(low, high, slider.normalizedValue);
    // }

    // void Update()
    // {
    //     slider.transform.position = Camera.main.WorldToScreenPoint(transform.parent.position + offSet);
    // }
    [SerializeField]
    private Vector3 localScale;
    [SerializeField]
    private float currentHp;
    void Start()
    {
        localScale = transform.localScale;
    }
    void Update()
    {
        localScale.x = PLHPWall.hitPointWall * 0.001f;
        transform.localScale = localScale;
        currentHp = PLHPWall.hitPointWall;
    }
}
