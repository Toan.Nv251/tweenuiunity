using UnityEngine;

public class PLMovePlayer : MonoBehaviour
{
    public static RectTransform rectPlayer;
    [SerializeField]
    private Vector2 speed = new Vector2(15, 15);
    private Vector2 movement;
    private Rigidbody2D rigidbodyComponent;
    public bool moveUp, moveDown;
    
    void Start()
    {
        rectPlayer = GetComponent<RectTransform>();
    }

    void Update()
    {
        MoveJoystick();
    }
    void FixedUpdate()
    {
        if (rigidbodyComponent == null)
        {
            rigidbodyComponent = GetComponent<Rigidbody2D>();
        }
        rigidbodyComponent.velocity = movement;
    }
    void MoveJoystick()
    {
        if(moveDown == false && moveUp == false)
        {
            movement = new Vector2(0, 0);
        }
        if(moveUp)
        {
            movement = new Vector2(0, speed.y);
        }
        else if(moveDown)
        {
            movement = new Vector2(0, -speed.y);
        }
    }
}
