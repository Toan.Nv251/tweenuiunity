using UnityEngine;

public class PLWeaponDamge : MonoBehaviour
{
    [SerializeField] 
    private float damage = 5f;
    public bool isEnemyShot = false;
    void OnTriggerEnter2D(Collider2D target){
        if(target.gameObject.tag == "Enemy")
        {
            target.gameObject.GetComponent<PLEnemyController>().takeHit(damage);
            Destroy(gameObject);
        }
    }
}
