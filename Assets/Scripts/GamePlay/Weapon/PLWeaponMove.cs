using UnityEngine;

public class PLWeaponMove : MonoBehaviour
{
    [SerializeField]
    private float speech;
    private RectTransform rect;

    void Start()
    {
        rect = GetComponent<RectTransform>();
        Destroy(gameObject, 10f);
    }
    void Update()
    {
        transform.Translate(new Vector3(speech, 0, 0));
    }
}
