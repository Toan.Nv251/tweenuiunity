using UnityEngine;

public class PLWeaponShot : MonoBehaviour
{
    [SerializeField]
    private Transform shotPrefab;
    [SerializeField]
    private float shootingRate = 0.25f;
    private float shotCoolDown;
    void Start()
    {
        shotCoolDown = 0f;
    }

    void Update()
    {
        
        if(shotCoolDown > 0)
        {
            shotCoolDown -= Time.deltaTime;
        }
    }

    public bool _canAttack
    {
        get
        {
            return shotCoolDown <= 0f;
        }
    }


    public void _attack(bool isEnemy)
    {
        if(_canAttack)
        {
            shotCoolDown = shootingRate;
            var shotTransform = Instantiate(shotPrefab) as Transform;
            shotTransform.position = transform.position;

            PLWeaponDamge shot = shotTransform.gameObject.GetComponent<PLWeaponDamge>();
        }
    }
}
