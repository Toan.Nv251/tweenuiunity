using UnityEngine;
using UnityEngine.UI;

public class PLNumberBomb : MonoBehaviour
{
    [SerializeField]
    private bool heal, bomb, rocket;
    private Text number;

    void Start()
    {
        number = GetComponent<Text>();
        if(heal == true && bomb == false && rocket == false)
        {
            number.text = "Rocket Number: " + PLRecoHealth.numberHealth;
        }
        if(heal == false && bomb == false && rocket == true)
        {
            number.text = "Rocket Number: " + PLObjectRocket.numberRocket;
        }
        if(heal == false && bomb == true && rocket == false)
        {
            number.text = "Bomb Number: " + PLObjectBomb.numberBomb;
        }
    }
    void Update()
    {
        if(heal == true && bomb == false && rocket == false)
        {
            number.text = "Rocket Number: " + PLRecoHealth.numberHealth;
        }
        if(heal == false && bomb == true && rocket == false)
        {
            number.text = "Bomb Number : " + PLObjectBomb.numberBomb;
        }
        if(heal == false && bomb == false && rocket == true)
        {
            number.text = "Rocket Number : " + PLObjectRocket.numberRocket;
        }
    }
}
