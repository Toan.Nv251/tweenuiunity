using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class PLRecoHealth : MonoBehaviour, IPointerDownHandler
{
    public static int numberHealth;
    void Start()
    {
        numberHealth = 10;
    }
    void Update()
    {
        
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if(numberHealth > 0)
        {
            PLHPWall.hitPointWall += 50;
            numberHealth -= 1;
        } 
    }
}
