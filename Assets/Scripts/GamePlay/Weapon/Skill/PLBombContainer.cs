using UnityEngine;
using UnityEngine.UI;

public class PLBombContainer : MonoBehaviour
{
    public static bool isFull;
    [SerializeField]
    private bool fullCurrent;
    public GameManager gameManager;
    public Image backGroundImage;

    void Start(){
        gameManager = GameManager.instance;
    }
    void Update()
    {
        fullCurrent = isFull;
    }

    public void OnTriggerEnter2D(Collider2D collision){
        if(collision.gameObject.tag == "Bomb")
        {
            if(gameManager.draggingObject != null && isFull == false){
            gameManager.currentContainer = this.gameObject;
            backGroundImage.enabled = true;
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision){
        if(collision.gameObject.tag == "Bomb")
        {
            gameManager.currentContainer = null;
            backGroundImage.enabled = false;
        }
    }
}
