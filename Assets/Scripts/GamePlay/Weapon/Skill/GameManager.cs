using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject draggingObject;
    public GameObject currentContainer;
    public GameObject canvasContainer;

    public static GameManager instance;

    void Awake(){
        instance = this;
    }
    public void PlaceObject(){
        if(draggingObject != null && currentContainer != null){
            Instantiate(draggingObject.GetComponent<PLBombDragg>().cardBomb.Object_Game, currentContainer.transform.position,transform.rotation,canvasContainer.transform.parent);
            //currentContainer.GetComponent<PLBombContainer>().isFull = true;
            PLBombContainer.isFull = true;
            PLObjectBomb.numberBomb -= 1;
        }
    }
}
