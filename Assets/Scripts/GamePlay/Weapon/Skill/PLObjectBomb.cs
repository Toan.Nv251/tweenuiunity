using UnityEngine;
using UnityEngine.EventSystems;

public class PLObjectBomb : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public GameObject object_Drag;
    public GameObject Object_Game;
    public Canvas canvas;
    private GameObject ObjectDragInstance;
    private GameManager gameManager;
    public static int numberBomb;
    [SerializeField]
    private int currentBomb;

    void Start(){
        numberBomb = 10;
        gameManager = GameManager.instance;
    }
    void Update()
    {
        currentBomb = numberBomb;
    }
    public void OnDrag(PointerEventData eventData){
        if(numberBomb > 0)
        {
            ObjectDragInstance.transform.position = Input.mousePosition;
        }
    }
    public void OnPointerDown(PointerEventData eventData){
        if(numberBomb > 0)
        {
            ObjectDragInstance = Instantiate(object_Drag, canvas.transform);
            ObjectDragInstance.transform.position = Input.mousePosition;
            ObjectDragInstance.GetComponent<PLBombDragg>().cardBomb = this;

            gameManager.draggingObject = ObjectDragInstance;
        }
    }
    public void OnPointerUp(PointerEventData eventData){
        if(numberBomb > 0)
        {
            gameManager.PlaceObject();
            gameManager.draggingObject = null;
            Destroy(ObjectDragInstance);
        }
    }
}
