using UnityEngine;

public class PLRocketSkill : MonoBehaviour
{
    [SerializeField]
    private float movement;
    [SerializeField]
    private float damage = 5f;

    void Start(){
        Destroy(gameObject, 3f);
    }

    void Update()
    {
        transform.Translate(new Vector3(movement, 0, 0));
    }
    private void OnTriggerEnter2D(Collider2D target){
        if(target.gameObject.tag == "Enemy")
        {
            target.gameObject.GetComponent<PLEnemyController>().takeHit(damage);
        }
    }
}
