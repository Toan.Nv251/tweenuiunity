using System.Collections;
using UnityEngine;

public class PLBombSkill : MonoBehaviour
{
    [SerializeField]
    private Vector3 LaunchOffset;
    [SerializeField]
    private float damage = 10f;
    [SerializeField]
    private float splashRange = 4f;
    [SerializeField]
    // private bool thrown;
    void Start()
    {
        // if(thrown){
        //     var direction = transform.right + Vector3.up;
        //     GetComponent<Rigidbody2D>().AddForce(direction * speech, ForceMode2D.Impulse);
        // }
        // transform.Translate(LaunchOffset);
    }
    void Update()
    {
        // if(!thrown){
        //     transform.position += transform.right * Time.deltaTime * speech;
        // }
        
    }
    IEnumerator OnTriggerEnter2D(Collider2D tag){
        yield return new WaitForSeconds(0.75f);

        var hitColliders = Physics2D.OverlapCircleAll(transform.position, splashRange);
        foreach(var hitCollider in hitColliders)
        {
            var enemy = hitCollider.GetComponent<PLEnemyController>();
            if(enemy)
            {
                    var closesPoint = hitCollider.ClosestPoint(transform.position);
                    var distance = Vector3.Distance(closesPoint, transform.position);
                    var damagePercent = Mathf.InverseLerp(splashRange, 0 , distance);
                    enemy.takeHit(damage);
            }
            Destroy(gameObject);
            PLBombContainer.isFull = false;
        }
    }

    void OnDrawGizmosSelected(){
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, splashRange);
    }
    
}
