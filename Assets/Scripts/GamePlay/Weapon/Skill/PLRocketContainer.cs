using UnityEngine;
using UnityEngine.UI;

public class PLRocketContainer : MonoBehaviour
{
    public bool isFull;
    public GameManagerRocket gameManager;
    public Image backGroundImage;

    void Start(){
        gameManager = GameManagerRocket.instanceRocket;
    }

    public void OnTriggerEnter2D(Collider2D collision){
        if(collision.gameObject.tag == "Rocket")
        {
            if(gameManager.draggingObjectRocket != null && isFull == false){
            gameManager.currentContainerRocket = this.gameObject;
            backGroundImage.enabled = true;
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision){
        if(collision.gameObject.tag == "Rocket")
        {
            gameManager.currentContainerRocket = null;
            backGroundImage.enabled = false;
        }
    }
}
