using UnityEngine;
using UnityEngine.EventSystems;

public class PLObjectRocket : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public GameObject object_Drag_Rocket;
    public GameObject Object_Game_rocket;
    public Canvas canvas;
    private GameObject ObjectDragInstanceRocket;
    private GameManagerRocket gameManager;
    public static int numberRocket;
    [SerializeField]
    private int currentRocket;

    void Start(){
        numberRocket = 10;
        gameManager = GameManagerRocket.instanceRocket;
    }
    void Update()
    {
        currentRocket = numberRocket;
    }
    public void OnDrag(PointerEventData eventData){
        if(numberRocket > 0)
        {
            ObjectDragInstanceRocket.transform.position = Input.mousePosition;
        }
    }
    public void OnPointerDown(PointerEventData eventData){
        if(numberRocket > 0)
        {
            ObjectDragInstanceRocket = Instantiate(object_Drag_Rocket, canvas.transform);
            ObjectDragInstanceRocket.transform.position = Input.mousePosition;
            ObjectDragInstanceRocket.GetComponent<PLRocketDragg>().cardRocket = this;

            gameManager.draggingObjectRocket = ObjectDragInstanceRocket;
        }
    }
    public void OnPointerUp(PointerEventData eventData){
        if(numberRocket > 0)
        {
            gameManager.PlaceObject();
            gameManager.draggingObjectRocket = null;
            Destroy(ObjectDragInstanceRocket);
        }
    }
}
