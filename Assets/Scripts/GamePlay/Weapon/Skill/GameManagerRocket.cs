using UnityEngine;

public class GameManagerRocket : MonoBehaviour
{
    public GameObject draggingObjectRocket;
    public GameObject currentContainerRocket;
    public GameObject canvasContainerRocket;

    public static GameManagerRocket instanceRocket;

    void Awake(){
        instanceRocket = this;
    }
    public void PlaceObject(){
        if(draggingObjectRocket != null && currentContainerRocket != null){
            Instantiate(draggingObjectRocket.GetComponent<PLRocketDragg>().cardRocket.Object_Game_rocket, currentContainerRocket.transform.position,transform.rotation,canvasContainerRocket.transform.parent);
            //currentContainer.GetComponent<PLBombContainer>().isFull = true;
            PLObjectRocket.numberRocket -= 1;
        }
    }
}
