using System.Collections.Generic;
using UnityEngine;

public class PLSpawer : MonoBehaviour
{
    [SerializeField]
    private float currentTime;
    [SerializeField]
    private float spawnTime;
    [SerializeField]
    private GameObject enemy1, enemy2,enemy3;
    private List<GameObject> listEnemy = new List<GameObject>();
    private int enemyRandom;
    public static int defaultValue;
    public int currentdefaValue;

    void Start()
    {
        defaultValue = 4;
        PlayerPrefs.SetInt("NumEnemy", defaultValue);
        spawnTime = 1f;
        currentTime = 0f;
        listEnemy.Add(enemy1);
        listEnemy.Add(enemy2);
        listEnemy.Add(enemy3);
    }

    void Update()
    {
        currentdefaValue = defaultValue;
        spawnEnemy();
        if(PlayerPrefs.GetInt("NumEnemy") > 10)
        {
            PlayerPrefs.SetInt("NumEnemy", 10);
        }
    }
    void spawnEnemy()
    {
        if(PlayerPrefs.GetInt("NumEnemy") >= 0)
        {
            currentTime += Time.deltaTime;
            if(currentTime >= spawnTime)
            {
                enemyRandom = Random.Range(1,4);
                if(enemyRandom == 1)
                {
                    Instantiate(enemy1,transform.position + new Vector3(0,Random.Range(-350,350)), transform.rotation,transform.parent);
                    defaultValue -= 1;
                    PlayerPrefs.SetInt("NumEnemy", defaultValue);
                }
                if(enemyRandom == 2)
                {
                    Instantiate(enemy2,transform.position + new Vector3(0,Random.Range(-350,350)), transform.rotation,transform.parent);
                    defaultValue -= 1;
                    PlayerPrefs.SetInt("NumEnemy", defaultValue);
                }
                if(enemyRandom == 3)
                {
                    Instantiate(enemy3,transform.position + new Vector3(0,Random.Range(-350,350)), transform.rotation,transform.parent);
                    defaultValue -= 1;
                    PlayerPrefs.SetInt("NumEnemy", defaultValue);
                }
                currentTime = 0;
            }
        }
    }
}
