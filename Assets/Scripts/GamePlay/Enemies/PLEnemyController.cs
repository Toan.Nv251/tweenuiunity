using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLEnemyController : MonoBehaviour
{
    public float health;
    public float movement;
    public bool isStopped;
    public int damageValue;
    public float damageCooldown;

    [SerializeField]
    private bool enemy1, enemy2, enemy3;
    [SerializeField]
    private RectTransform rect;
    [SerializeField]
    private GameObject fireBall;
    [SerializeField]
    private float attackTime;
    [SerializeField]
    private float attackCoolDown;
    [SerializeField]
    private GameObject spawner;
    private float randomX;
    private float maxY = 360f, minY = -433f;
    void Start()
    {
        randomX = Random.Range(400f, 100f);
        rect = GetComponent<RectTransform>();
    }

    void Update()
    {
        Vector2 temp = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y);
        if(temp.y < minY)
        {
            temp.y = minY;
        }
        if(temp.y > maxY)
        {
            temp.y = maxY;
        }
        rect.anchoredPosition = temp;
        if(enemy1 == true && enemy2 == false && enemy3 == false)
        {
            if(rect.anchoredPosition.y < PLMovePlayer.rectPlayer.anchoredPosition.y + 100 && rect.anchoredPosition.y > PLMovePlayer.rectPlayer.anchoredPosition.y - 100)
            {
                if(rect.anchoredPosition.y > 0)
                {
                    transform.Translate(new Vector3(0, -1 , 0));
                }
                else if(rect.anchoredPosition.y <= 0)
                {
                    transform.Translate(new Vector3(0, 1 , 0));
                }
            }
            
            transform.Translate(new Vector3(movement * -1, 0 , 0));
        }
        if(enemy1 == false && enemy2 == true && enemy3 == false)
        {
            if(!isStopped)
            {
                transform.Translate(new Vector3(movement * -1, 0 , 0));
            }
        }
        if(enemy1 == false && enemy2 == false && enemy3 == true)
        {
            if(rect.anchoredPosition.x > randomX - 800 )
            {
                transform.Translate(new Vector3(movement * -1, 0 , 0));
            }
            else
            {
                transform.Translate(new Vector3(0, 0 , 0));
                if(attackTime <= Time.time)
                {
                    Instantiate(fireBall,transform.position, transform.rotation,transform.parent);
                    attackTime = Time.time + attackCoolDown;
                }
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D target){
        if(target.gameObject.tag == "Wall")
        {
            if(enemy1 == false && enemy2 == true && enemy3 == false)
            {
                StartCoroutine(attack(target));
                isStopped = true;
            }
            if(enemy1 == true && enemy2 == false && enemy3 == false)
            {
                target.gameObject.GetComponent<PLHPWall>().takeHit(damageValue);
                takeHit(1000f);
            }
        }
        if(target.gameObject.tag == "Weapon1")
        {
            rect.anchoredPosition = new Vector3(rect.anchoredPosition.x + 30f, rect.anchoredPosition.y);
        }
    }
    public void OnTriggerExit2D(Collider2D target)
    {
        if(target.gameObject.tag == "Wall")
        {
            isStopped = false;
        }
    }

    IEnumerator attack(Collider2D target){
        if(target == null)
        {
            isStopped = false;
        }
        else
        {
            target.gameObject.GetComponent<PLHPWall>().takeHit(damageValue);
            yield return new WaitForSeconds(damageCooldown);
            StartCoroutine(attack(target));
        }
    }
    public void takeHit(float damage){
        if(isStopped == true)
        {
            health -= damage/5;
        }
        else
        {
            health -= damage;
        }
        if(health <= 0){
            Destroy(gameObject);
            health += 10;
            PLSpawer.defaultValue += 1;
            PlayerPrefs.SetInt("NumEnemy", PLSpawer.defaultValue);
        }
    }
}
