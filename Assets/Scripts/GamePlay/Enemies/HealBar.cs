using UnityEngine;
using UnityEngine.UI;

public class HealBar : MonoBehaviour
{
    [SerializeField]
    private Vector3 localScale;
    public PLEnemyController enemy;
    void Start()
    {
        localScale = transform.localScale;
    }
    void Update()
    {
        localScale.x = enemy.health * 0.1f;
        transform.localScale = localScale;
    }
}
