using UnityEngine;

public class PLEnemyWeapon : MonoBehaviour
{
    [SerializeField]
    private float movement;
    [SerializeField]
    private float fireDamage;

    void Start()
    {
        Destroy(gameObject, 6f);
    }
    void Update()
    {
        transform.Translate(new Vector3(movement * -1, 0, 0));
    }
    public void OnTriggerEnter2D(Collider2D target){
        if(target.gameObject.tag == "Wall")
        {
            target.gameObject.GetComponent<PLHPWall>().takeHit(fireDamage);
            Destroy(gameObject);
        }
    }
    
}
