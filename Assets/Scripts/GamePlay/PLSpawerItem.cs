using UnityEngine;

public class PLSpawerItem : MonoBehaviour
{
    [SerializeField]
    private GameObject itemDrop;
    [SerializeField]
    private float coolDownDrop, currentDrop;
    void Update()
    {
        if(currentDrop <= Time.time)
        {
            Instantiate(itemDrop, transform.position, transform.rotation, transform.parent);
            currentDrop = Time.time + coolDownDrop;
        }
    }
}
