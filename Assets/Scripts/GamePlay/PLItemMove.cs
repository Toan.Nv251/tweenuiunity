using UnityEngine;
using UnityEngine.EventSystems;
public class PLItemMove : MonoBehaviour,IPointerDownHandler
{
    [SerializeField]
    private int numberItem;
    void Start()
    {
        numberItem = Random.Range(1,4);
        Destroy(gameObject, 10f);
    }
    void Update()
    {
        transform.Translate(new Vector3(0, -0.5f, 0));
    }
    public void OnPointerDown(PointerEventData eventData){
        if(numberItem == 1)
        {
            PLObjectBomb.numberBomb += 1;
            Destroy(gameObject);
        }
        if(numberItem == 2)
        {
            PLObjectRocket.numberRocket += 1;
            Destroy(gameObject);
        }
        if(numberItem == 3)
        {
            PLRecoHealth.numberHealth += 1;
            Destroy(gameObject);
        }
    }
}
