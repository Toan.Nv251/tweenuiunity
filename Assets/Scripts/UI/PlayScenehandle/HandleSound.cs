using UnityEngine;


public class HandleSound : MonoBehaviour
{
    public GameObject objectSound;
    private AudioSource soundSource;

    void Start() {
        objectSound = GameObject.FindGameObjectWithTag("GameSound");
        soundSource = objectSound.GetComponent<AudioSource>();
    }
    public void _buttonSoundOn() {
         if(PlayerPrefs.GetString("OnOffSound") != "OffSound"){
            soundSource.Play();
        }
    }
}
