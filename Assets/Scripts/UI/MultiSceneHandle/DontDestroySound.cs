using UnityEngine;

public class DontDestroySound : MonoBehaviour
{
    void Awake() {
        GameObject[] audioSourceTag = GameObject.FindGameObjectsWithTag("GameSound");
        if(audioSourceTag.Length > 1) {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
