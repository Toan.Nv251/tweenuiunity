using UnityEngine;
using UnityEngine.UI;

public class SoundHandleMusicMulScene : MonoBehaviour
{
    public Toggle toggleSound;
    public GameObject objectSound;
    private string soundOnOff = "OnSound";
    private AudioSource soundSource;

    void Start() {
        objectSound = GameObject.FindGameObjectWithTag("GameSound");
        soundSource = objectSound.GetComponent<AudioSource>();
        if(PlayerPrefs.GetString("OnOffSound") == "OffSound"){
            toggleSound.isOn = false;
        }else
        {
            toggleSound.isOn = true;
        }
        //soundOnOff = PlayerPrefs.GetString("OnOffSound");
    }
    public void _buttonSound() {
        if(toggleSound.isOn == true) {
            soundOnOff = "OnSound";
            PlayerPrefs.SetString("OnOffSound", soundOnOff);
        }
        if(toggleSound.isOn == false) {
            soundOnOff = "OffSound";
            PlayerPrefs.SetString("OnOffSound", soundOnOff);
        }
    }
    public void _buttonSoundOn() {
         if(PlayerPrefs.GetString("OnOffSound") != "OffSound"){
            soundSource.Play();
        }
    }
}
