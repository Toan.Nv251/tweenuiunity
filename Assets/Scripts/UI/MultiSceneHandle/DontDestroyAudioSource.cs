using UnityEngine;

public class DontDestroyAudioSource : MonoBehaviour
{
    void Awake() {
        GameObject[] audioSourceTag = GameObject.FindGameObjectsWithTag("GameMusic");
        if(audioSourceTag.Length > 1) {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
