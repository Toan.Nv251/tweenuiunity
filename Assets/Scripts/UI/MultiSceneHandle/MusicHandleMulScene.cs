using UnityEngine;
using UnityEngine.UI;
public class MusicHandleMulScene : MonoBehaviour
{
    public GameObject objectMusic;
    public Toggle toggleMusic;

    private string musicVolume = "OnMusic";
    private AudioSource audioSource;

    void Start() {
        objectMusic = GameObject.FindGameObjectWithTag("GameMusic");
        audioSource = objectMusic.GetComponent<AudioSource>();
        musicVolume = PlayerPrefs.GetString("OnOffMusic");
    }


    public void handleMusic() {
        if(toggleMusic.isOn == true) {
            musicVolume = "OnMusic";
            PlayerPrefs.SetString("OnOffMusic", musicVolume);
            audioSource.Play();
        }else
        {
            musicVolume = "OffMusic";
            PlayerPrefs.SetString("OnOffMusic", musicVolume);
            audioSource.Pause();
        }
    }
}
