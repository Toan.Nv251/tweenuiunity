using UnityEngine;
using UnityEngine.UI;
public class FixBugToggles : MonoBehaviour
{
    public Toggle toggleFixMusic;

    void Start() {
        //Debug.Log(PlayerPrefs.GetString("OnOffMusic"));
        if(PlayerPrefs.GetString("OnOffMusic") == "OnMusic"){
            toggleFixMusic.isOn = true;
        }
        if(PlayerPrefs.GetString("OnOffMusic") == "OffMusic"){
            toggleFixMusic.isOn = false;
        }
    }
}
