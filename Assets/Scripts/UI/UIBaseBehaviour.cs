using System;
using System.Collections;
using UnityEngine;

public class UIBaseBehaviour : MonoBehaviour
{
    public Coroutine Invoke(Action action, float time){
        return StartCoroutine(InvokeAftertime(action, time));
    }
    private IEnumerator InvokeAftertime(Action action, float time){
        yield return new WaitForSeconds(time);
        action?.Invoke();
    }
}
