using UnityEngine;
using DG.Tweening;

public class UINavigation : MonoBehaviour
{
    public GameObject gameOverPannel;
    public GameObject spawner;
    public GameObject player;
    public GameObject buttonUp, buttonDown;
    public RectTransform mainMenu, setting,leaderBoard;

    void Start() {
        mainMenu.DOAnchorPos(Vector2.zero, 0f);
    }

    //Navigation Main menu
    public void _settingButton() {
        mainMenu.DOAnchorPos(new Vector2(-800,0), 0.25f);
        setting.DOAnchorPos(new Vector2(0,0), 0.25f);
    }
    public void _leaderBoardButton() {
        mainMenu.DOAnchorPos(new Vector2(-800,0), 0.25f);
        leaderBoard.DOAnchorPos(new Vector2(0,0), 0.25f);
    }
    public void _backButton() {
        mainMenu.DOAnchorPos(new Vector2(0,0), 0.25f);
        setting.DOAnchorPos(new Vector2(800,0), 0.25f);
        leaderBoard.DOAnchorPos(new Vector2(0,-1200), 0.25f);
    }

    //Navigation Scene
    public void _goPlayScene() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("PlayScene");
    }
    public void _backMainMenuScene() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
    }
    //restart play scene
    public void _restartPlayScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("PlayScene");
    }
    public void _PauseGame()
    {
        spawner.SetActive(false);
        player.SetActive(false);
        buttonDown.SetActive(false);
        buttonUp.SetActive(false);
        gameOverPannel.SetActive(true);
    }
    public void _ResumeGame()
    {
        spawner.SetActive(true);
        player.SetActive(true);
        buttonDown.SetActive(true);
        buttonUp.SetActive(true);
        gameOverPannel.SetActive(false);
    }
}
