using UnityEngine;
using System;
using UnityEngine.UI;

public class UILeaderBoardJson : MonoBehaviour
{
    public TextAsset myJson;
    [Serializable]
    public class myPoint{
        public string name;
        public string point;
    }
    public myPoint[] myObject;
    void Start(){
        myObject = JsonHelper.GetArray<myPoint>(myJson.text);
        _DrawUI ();
    }
    void _DrawUI ()
	{
		GameObject buttonTemplate = transform.GetChild (0).gameObject;
		GameObject g;

		int N = myObject.Length;

		for (int i = 0; i < N; i++) {
			g = Instantiate (buttonTemplate, transform);
			g.transform.GetChild (0).GetComponent <Text> ().text = myObject [i].name;
			g.transform.GetChild (1).GetComponent <Text> ().text = myObject [i].point;
            g.transform.GetChild (3).GetComponent <Text> ().text = "" + (i+1);
		}
		Destroy (buttonTemplate);
	}
}
