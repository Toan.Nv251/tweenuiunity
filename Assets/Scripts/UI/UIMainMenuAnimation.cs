using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIMainMenuAnimation : UIBaseBehaviour
{
    //Add button
    [SerializeField] private Button playButton,cupButton,gamePadButton,setButton,comunButton;

    [SerializeField] private Button musicButton, volumButton, gmailButton, tbutton, facebookButton;
    [SerializeField] private CanvasGroup title;

    [SerializeField] private GameObject titleImage;

    private float INITIAL_DELAY = 1f;
    private const float DELAY_BETWEEN_BUTTONS = 0.5f;


    private List<Button> buttons = new List<Button>();
    private List<Sequence> animationSequences = new List<Sequence>();
    private void Awake(){
        //add button to array buttons
        buttons.Add(playButton);
        buttons.Add(cupButton);
        buttons.Add(gamePadButton);
        buttons.Add(setButton);
        buttons.Add(comunButton);
        //run animation
        AnimateTitle();
        AnimateButtons();
    }


    private void AnimateButtons(){
        //transform button to default position
        titleImage.transform.localScale = Vector3.zero;
        musicButton.transform.localScale = Vector3.zero;
        volumButton.transform.localScale = Vector3.zero;
        gmailButton.transform.localScale = Vector3.zero;
        tbutton.transform.localScale = Vector3.zero;
        facebookButton.transform.localScale = Vector3.zero;
        //animation button
        Invoke(() => titleImage.transform.DOScale(1, 1f), 0.3f);
        Invoke(() => musicButton.transform.DOScale(1, 0.3f), 0.3f);
        Invoke(() => volumButton.transform.DOScale(1, 0.3f), 0.3f);
        Invoke(() => gmailButton.transform.DOScale(1, 0.3f), 0.3f);
        Invoke(() => tbutton.transform.DOScale(1, 0.3f), 0.3f);
        Invoke(() => facebookButton.transform.DOScale(1, 0.3f), 0.3f);
        //animation main button
        for(int i = 0; i < 5; i++){
            buttons[i].transform.localScale = Vector3.zero;
            AnimateButtons(i, INITIAL_DELAY + DELAY_BETWEEN_BUTTONS * i);
        }
    }
    //animation main button
    private void AnimateButtons(int index, float delay){
        if(animationSequences.Count >= index){
            animationSequences.Add(DOTween.Sequence());
        }else{
            if(animationSequences[index].IsPlaying()){
                animationSequences[index].Kill(true);
            }
        }
        var seq = animationSequences[index];
        var button = buttons[index];
        seq.Append(button.transform.DOScale(1, 0.3f));
        seq.Append(button.transform.DOPunchScale(Vector3.one * 0.6f, 0.8f, 6, 0.7f)).SetEase(Ease.OutCirc);
        seq.PrependInterval(delay);
    }
    //animation title
    private void AnimateTitle()
	{
		title.alpha = 0f;
		title.DOFade(1f, 1.8f).SetEase(Ease.InQuint);
	}
}
